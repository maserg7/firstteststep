
import org.junit.jupiter.api.Test;
import pages.AuthorizationToMailPage;
import pages.IdPassportYandexPage;
import pages.MailPage;
import pages.MainPage;


public class YandexMailTest extends BaseTest {
    @Test
    public void checkYandexEmail() {
        AuthorizationToMailPage authorizationToMailPage = MainPage.open()
                .goToMail();

        IdPassportYandexPage idPassportYandexPage = authorizationToMailPage.click();

        idPassportYandexPage.chooseAuthorizationMethod();
        idPassportYandexPage.enterEmail();
        //idPassportYandexPage.confirmNumber();

        MailPage mailPage = idPassportYandexPage.enterPassword();

        mailPage.checkVisionMailPage();
        mailPage.shouldMarkSteam();
    }
}
