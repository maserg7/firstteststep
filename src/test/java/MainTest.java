import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;


public class MainTest {
    @BeforeAll
    public static void start() {
        WebDriverManager.chromedriver().setup();
        Configuration.browser = "chrome";
        Configuration.driverManagerEnabled = true;
        Configuration.holdBrowserOpen = true;
    }
    @Test
    public void shouldSearch() {
        System.out.println(Main.findShamrockNumber());
        Assertions.assertNotNull( Main.findShamrockNumber());
    }
//    @Test
//    public void shouldOpenShamrockSite(){
//
//    }
    @AfterAll
    public static void tearDown() {
        Selenide.closeWebDriver();
    }
}
