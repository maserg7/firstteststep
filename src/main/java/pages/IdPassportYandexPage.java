package pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import lombok.extern.log4j.Log4j2;

import static com.codeborne.selenide.Selenide.$x;
@Log4j2
public class IdPassportYandexPage {
    private final SelenideElement INPUT_TEXT_EMAIL = $x("//input[@type='text']");
    private final SelenideElement INPUT_TEXT_PASSWORD = $x("//input[@name='passwd']");
//    private final SelenideElement CONFIRM_NUMBER_OF_PHONE = $x("//button[@type='submit']");
    private final SelenideElement CHOOSE_MAIL_AUTHORIZATION = $x("//button[@data-type='login']");

    private final String EMAIL = "maserg7gmail.com@yandex.ru";
    private final String PASSWORD = "Mas0170710";
    MailPage mailPage = new MailPage();
    @Step("Проверяем видимость кнопки способа авторизации через E-Mail")
    public IdPassportYandexPage checkVisionButton() {
        log.info("Проверяем видимость кнопки способа авторизации через E-Mail");
        INPUT_TEXT_EMAIL.as("Кнопка войти должна быть видна").shouldBe(Condition.visible);
        return this;
    }
    @Step("Выбираем способ авторизации нажатием кнопки Почта")
    public void chooseAuthorizationMethod () {
        log.info("Выбираем способ авторизации нажатием кнопки Почта");
        CHOOSE_MAIL_AUTHORIZATION.click();
    }
    @Step("Вводим адрес эл.почты в специальное поле")
    public void enterEmail() {
        log.info("Вводим адрес эл.почты в специальное поле");
        INPUT_TEXT_EMAIL.setValue(EMAIL).pressEnter();
    }
    @Step("Вводим пароль от эл.почты в специальное поле и переходим на след.вкладку")
    public MailPage enterPassword() {
        log.info("Вводим пароль от эл.почты в специальное поле и переходим на след.вкладку");
        INPUT_TEXT_PASSWORD.setValue(PASSWORD).pressEnter();
        return mailPage.CheckVisionElement();
    }
//    public MailPage confirmNumber() {
//        confirmNumberOfPhone.click();
//        return mailPage;
//  }
}
