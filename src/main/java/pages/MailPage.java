package pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import lombok.extern.log4j.Log4j2;
import org.openqa.selenium.By;
import static com.codeborne.selenide.Selenide.*;

@Log4j2
public class MailPage {
    SelenideElement table = $(By.xpath("//div[@class='Folder-m__root--iLgY8 qa-LeftColumn-Folder " +
            "Folder-m__selected--QRoPQ qa-LeftColumn-Folder_selected']"));
    ElementsCollection mail = $$(By.xpath("//div[@class='mail-MessageSnippet-Content']")).filterBy(Condition.text("Steam"));


    @Step("Проверяем видимость элемента для установки галочки(выбор письма)")
    public MailPage CheckVisionElement () {
        log.info("Проверяем видимость элемента для установки галочки(выбор письма)");
        SelenideElement x = $x("//span[@class='_nb-checkbox-flag _nb-checkbox-normal-flag']");
        x.as("Элемент виден").shouldBe(Condition.visible);
        return this;
    }
    @Step("Страница почты должна быть видна")
    public MailPage checkVisionMailPage () {
        log.info("Страница почты должна быть видна");
        table.as("Страница почты должна быть видна").shouldBe(Condition.visible);
        return this;
    }
    @Step("Помечаем нужные нам письма галочкой выбора")
    public void shouldMarkSteam() {
        log.info("Помечаем нужные нам письма галочкой выбора");
            for(SelenideElement l: mail) {
                l.$(By.cssSelector("._nb-checkbox-normal-flag")).click();
        }
    }
}
