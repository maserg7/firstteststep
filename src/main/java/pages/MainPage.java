package pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import lombok.extern.log4j.Log4j2;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
@Log4j2
public class MainPage {
    AuthorizationToMailPage authorizationToMailPage = new AuthorizationToMailPage();
    SelenideElement mailButton = $(By.id("services-big-item-mail-title"));

    @Step("Открываем страницу сервисов Яндекс")
    public static MainPage open() {
        log.info("Открываем страницу сервисов Яндекс");
        Selenide.open("https://yandex.ru/all?from=tabbar");
        return Selenide.page(MainPage.class);
    }
    @Step("Нажимаем кнопку Почта для перехода на новую страницу")
    public AuthorizationToMailPage goToMail() {
        log.info("Нажимаем кнопку Почта для перехода на новую страницу");
        mailButton.as("Проверяем видимость кнопки Почта").shouldBe(Condition.visible).click();
        return authorizationToMailPage.checkVisionEnterButton();
    }
}
