package pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import lombok.extern.log4j.Log4j2;
import org.openqa.selenium.By;
import static com.codeborne.selenide.Selenide.$;

@Log4j2
public class AuthorizationToMailPage {
    SelenideElement buttonOfMail = $(By.xpath("//a[@class='Button2 Button2_type_link Button2_view_default Button2_size_m']"));
    IdPassportYandexPage idPassportYandexPage = new IdPassportYandexPage();

    @Step("Проверяем видимость кнопки")
    public AuthorizationToMailPage checkVisionEnterButton() {
        log.info("Проверяем видимость кнопки");
        buttonOfMail.as("Кнопка войти должна быть видна").shouldBe(Condition.visible);
        return this;
    }

    @Step("Нажимаем на кнопку войти и проверяем отображение следующей страницы")
    public IdPassportYandexPage click() {
        log.info("Кликаем на кнопку войти");
        buttonOfMail.click();
        return idPassportYandexPage.checkVisionButton();
    }
}
