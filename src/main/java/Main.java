import com.codeborne.selenide.Condition;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.*;

public class Main {

    public static String findShamrockNumber() {
        open("http://google.com");
       $(By.xpath("//input[@class='gLFyf']")).shouldBe(Condition.visible).
               setValue("Shamrock Королев сайт").pressEnter();
       $$(By.xpath("//h3[@class='LC20lb MBeuO DKV0Md']")).
               find(Condition.text("Традиционный ирландский паб в Королеве - Shamrock")).click();
       String number = $(By.xpath("//*[@id=\"additional-phone-info\"]/div/div[2]/div[2]/div/p[1]/b/a")).getAttribute("href");
       return number;
    }

}
